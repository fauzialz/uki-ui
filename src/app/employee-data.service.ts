import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Config } from './base/config';

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {
  constructor(
    private http: HttpClient
  ) { }

  getAllEmployee(){
    return this.http.get(Config.URL + '/api/employees');
  }

  getEmployeeById(id){
    return this.http.get(Config.URL + '/api/employees/' + id);
  }

  postEmployee(employee){
    const emData: any = {
      name: employee.name,
      department: employee.department
    };
    return this.http.post(Config.URL + '/api/employees', emData);
  }

  putEmployee(employee){
    const emData: any = {
      name: employee.name,
      department: employee.department
    };
    return this.http.put(Config.URL + '/api/employees/' + employee.id, emData)
  }

  delEmployee(id){
    return this.http.delete(Config.URL + '/api/employees/' + id)
  }
}
